<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Behavioral Engine</title>
		<meta name="viewport" content="initial-scale = 1.0, user-scalable=no">
		<link href="http://fonts.googleapis.com/css?family=Roboto:100&subset=latin,cyrillic-ext,latin-ext" type="text/css" rel="stylesheet">
		<link rel="Stylesheet" type="text/css" href="css/bootstrap.css" />
		<link rel="Stylesheet" type="text/css" href="css/style.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script> -->
		<script defer src="js/main.js"></script>
		<!-- <script defer>

			function initialize() {
				var myLatlng = new google.maps.LatLng(52.2341193, 21.0158484, 17);
				var mapOptions = {
					zoom: 17,
					center: myLatlng,
					panControl: false,
					zoomControl: false,
					scaleControl: false,
					draggable: false,
					scrollwheel: false,
					disableDoubleClickZoom: true

				};

				var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

				var contentString = '<div id="content">' +
						'<div id="siteNotice">' +
						'</div>' +
						'<div class="subhead textleft">Dane kontaktowe</div><br>' +
						'<p class="bold">Dane korespondencyjne</p>' +
						'<p>	Cloud Technologies S.A.<br>' +
						'ul. Górskiego 6/24<br>' +
						'00-033 Warszawa<br>' +
						'</p><br>' +
						'<p class="bold">Dane rejestrowe</p>' +
						'<p>	Cloud Technologies S.A.<br>' +
						'ul. Żeromskiego 7, 05-075 Warszawa<br>' +
						'BRE Bank: 55 1140 1010 0000 5357 1200 1001<br>' +
						'KRS: 0000405842, NIP: 9522106251, REGON: 142886479<br>' +
						'Sąd Rejonowy dla m.st. Warszawy, XIII Wydział Gospodarczy KRS<br>' +
						'Kapitał zakładowy: 330.000,00 zł<br>' +
						'Biuro Pn-Pt 9:00 - 17:00<br>' +
						'<br>' +
						'T: +48 225353050<br>' +
						'F: +48 225353070<br>' +
						'E: biuro@cloudtechnologies.pl<br>' +
						'</p>' +
						'</div>';

				var infowindow = new google.maps.InfoWindow({
					content: contentString,
					maxWidth: 500
				});

				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Cloud Technologies S.A.'
				});

				google.maps.event.addListener(marker, 'click', function () {
					infowindow.open(map, marker);
				});


			}

			google.maps.event.addDomListener(window, 'load', initialize);

			/* function loadScript() {
			 var script = document.createElement('script');
			 script.type = 'text/javascript';
			 script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp' +
			 '&signed_in=true&callback=initialize';
			 document.body.appendChild(script);
			 }
			 
			 window.onload = loadScript; */

		</script> -->
    </head>
    <body>
		<script>
			// asynchronouns loading google map api
			function loadScript(src, callback) {

				var script = document.createElement("script");
				script.type = "text/javascript";
				if (callback)
					script.onload = callback;
				document.getElementsByTagName("head")[0].appendChild(script);
				script.src = src;
			}


			loadScript('http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=initialize');


			function initialize() {
				var myLatlng = new google.maps.LatLng(52.2341193, 21.0158484, 17);
				var mapOptions = {
					zoom: 17,
					center: myLatlng,
					panControl: false,
					zoomControl: false,
					scaleControl: false,
					draggable: false,
					scrollwheel: false,
					disableDoubleClickZoom: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};

				map = new google.maps.Map(document.getElementById('map-canvas'),
						mapOptions, marker);
						
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title: 'Cloud Technologies S.A.'
				});
			}

		</script>
		<header>
			<nav>
				<ul>
					<li><a href="#slide1" class="active"></a></li>
					<li><a href="#slide2"></a></li>
					<li><a href="#slide3"></a></li>
					<li><a href="#slide4"></a></li>
					<li><a href="#slide5"></a></li>
					<li><a href="#slide6"></a></li>
					<li><a href="#slide7"></a></li>
					<li><a href="#slide8"></a></li>
					<li><a href="#slide9"></a></li>
					<li><a href="#slide10"></a></li>
					<li><a href="#slide11"></a></li>
				</ul>
			</nav>
		</header>
		<main role="main">
			<section id="slide1" class="slide">
				<div id="cloud">
					<div class="hide-cloud-circle1"></div>
					<div class="hide-cloud-circle2"></div>
					<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
						 viewBox="0 0 595.3 600" enable-background="new 0 0 595.3 841.9" xml:space="preserve">
					<g>
					<path fill="#0080FF" d="M244,264c31.6,0,60,13.4,79.9,34.9c14.4-8.3,31.1-13,48.9-13c40,0,74.4,23.9,89.7,58.3
						  c4.9-1,9.9-1.5,15.1-1.5c42.2,0,76.4,34.2,76.4,76.4s-34.2,76.4-76.4,76.4c-1.7,0-3.3-0.1-5-0.2c-14.6,36-49.8,61.4-91.1,61.4
						  c-17.8,0-34.5-4.8-48.9-13c-19.9,21.4-48.4,34.9-80,34.9c-47.3,0-87.5-30.1-102.7-72.1c-6.3,1.4-12.8,2.3-19.6,2.3
						  c-48.2,0-87.3-39.1-87.3-87.3c0-48.2,39.1-87.3,87.3-87.3c3.9,0,7.6,0.3,11.4,0.8C157.4,293.4,197.2,264,244,264 M244,250.9
						  c-47.2,0-90.4,27.7-110.5,69.9c-1,0-2-0.1-3-0.1C75.1,320.7,30,365.8,30,421.2s45.1,100.4,100.4,100.4c3.8,0,7.6-0.2,11.5-0.7
						  c19.9,42.6,63.1,70.6,110.8,70.6c30.5,0,60-11.5,82.4-32c14.5,6.7,30.4,10.1,46.4,10.1c42.5,0,80.6-23.7,99.4-61.2
						  c47.8-1.8,86.2-41.2,86.2-89.5c0-49.4-40.2-89.5-89.5-89.5c-2.5,0-5.1,0.1-7.6,0.3c-19.5-35-56.7-57.1-97.2-57.1
						  c-16,0-31.9,3.5-46.4,10.1C304,262.4,274.5,250.9,244,250.9L244,250.9z"/>
					</g>
					</svg>
					<div class="number-cols">
						<div class="number-col current first">
							<span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>1</span>
						</div>
						<div class="number-col next">
							<span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col current second">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span>
						</div>
						<div class="number-col second next">
							<span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span><span>1</span><span>0</span><span>1</span><span>0</span><span>0</span>
						</div>
						<div class="number-col current third">
							<span>0</span><span>1</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col third next">
							<span>1</span><span>1</span><span>0</span><span>0</span><span>0</span><span>1</span><span>0</span><span>0</span><span>0</span><span>1</span><span>1</span>
						</div>
						<div class="number-col current fourth">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span>
						</div>
						<div class="number-col fourth next">
							<span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>0</span><span>0</span><span>1</span>
						</div>
						<div class="number-col current fifth">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>0</span>
						</div>
						<div class="number-col fifth next">
							<span>1</span><span>1</span><span>0</span><span>1</span><span>1</span><span>0</span><span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col current sixth">
							<span>0</span><span>0</span><span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>1</span><span>0</span><span>0</span><span>1</span><span>1</span>
						</div>
						<div class="number-col sixth next">
							<span>1</span><span>1</span><span>1</span><span>0</span><span>0</span><span>1</span><span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>1</span>
						</div>
						<div class="number-col current seventh">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span>
						</div>
						<div class="number-col seventh next">
							<span>0</span><span>1</span><span>1</span><span>0</span><span>0</span><span>1</span><span>0</span><span>1</span><span>1</span><span>0</span><span>1</span><span>1</span>
						</div>
						<div class="number-col current eighth">
							<span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col eighth next">
							<span>1</span><span>1</span><span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span><span>0</span><span>0</span>
						</div>
						<div class="number-col current ninth">
							<span>0</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>1</span><span>1</span><span>1</span>
						</div>
						<div class="number-col ninth next">
							<span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>0</span><span>0</span><span>0</span>
						</div>
						<div class="number-col current tenth">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col tenth next">
							<span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>0</span><span>0</span><span>1</span><span>0</span><span>1</span>
						</div>
						<div class="number-col current eleventh">
							<span>1</span><span>0</span><span>0</span><span>0</span><span>0</span><span>0</span><span>0</span><span>1</span><span>1</span><span>1</span><span>1</span><span>1</span>
						</div>
						<div class="number-col eleventh next">
							<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>1</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span>
						</div>
					</div>

				</div>

			</section>
			<section id="slide2" class="slide">
				<div class="top gray">
					<div class="top-title">
						<h2>Data Management<br>Platform</h2>
					</div>
				</div>
				<!-- SVG triangle
				<div class="top">
					<div class="top-background">
						<svg>
						<polygon points="0,0 600,0 120,160" style="fill:#eee;stroke-width:0" />
						Sorry, your browser does not support inline SVG.
						</svg>
						<div class="top-title">
							<h2>Data Management<br>Platform</h2>
						</div>
					</div>
				</div> -->
				<div class="content">
					
					<div class="page-description">
						<h2 class="yellow">Największy zbiór danych o Internautach w chmurze</h2>
						<ul>
							<li>Zwiększa skuteczność reklamy internetowej</li>
							<li>Pozwala zdobyć wiedzę o użytkownikach</li>
							<li>Optymalizuje sprzedaż on-line</li>
							<li>Wzbogaca systemy CRM</li>
						</ul>
					</div>
				</div>
			</section>
			<section id="slide3" class="slide">
				<!-- SVG triangle on bottom
				<div class="top-on-bottom">
					<div class="top-background">
						<svg>
						<polygon points="0,160 600,160 480,0" style="fill:#0080ff;stroke-width:0" />
						Sorry, your browser does not support inline SVG.
						</svg>
						<div class="top-title">
							<h2>1st<br>party data</h2>
						</div>
					</div>
				</div>
				-->
				<div class="main-container row">
					<div class="scheme-1st-party">
						<div class="icon-first">
							<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 191.1 211.5" enable-background="new 0 0 191.1 211.5" xml:space="preserve">
							<path fill="#0080FF" d="M128.2,119.6H26.9c-1.6,0-3,1.3-3,3c0,1.6,1.3,3,3,3h101.3c1.6,0,3-1.3,3-3
								  C131.2,120.9,129.9,119.6,128.2,119.6z"/>
							<path fill="#0080FF" d="M128.2,138.4H26.9c-1.6,0-3,1.3-3,3c0,1.6,1.3,3,3,3h101.3c1.6,0,3-1.3,3-3
								  C131.2,139.7,129.9,138.4,128.2,138.4z"/>
							<path fill="#0080FF" d="M128.2,155.7H26.9c-1.6,0-3,1.3-3,3c0,1.6,1.3,3,3,3h101.3c1.6,0,3-1.3,3-3
								  C131.2,157,129.9,155.7,128.2,155.7z"/>
							<path fill="#0080FF" d="M128.2,174.4H26.9c-1.6,0-3,1.3-3,3c0,1.6,1.3,3,3,3h101.3c1.6,0,3-1.3,3-3
								  C131.2,175.8,129.9,174.4,128.2,174.4z"/>
							<g>
							<path fill="#FFCA24" d="M190.5,46.5c-0.1-0.1-0.1-0.1-0.2-0.1c-0.1-0.1-0.3-0.1-0.4-0.1c0,0-0.1,0-0.1,0l-9.2,1
								  c-0.4-2.8-1-5.5-1.9-8.2l8.6-3.2c0.3-0.1,0.5-0.3,0.6-0.5c0.1-0.2,0.1-0.5,0-0.8c-2-5.4-4.9-10.5-8.5-15c-0.1-0.1-0.2-0.2-0.4-0.3
								  c-0.1-0.1-0.3-0.1-0.4-0.1c-0.2,0-0.5,0.1-0.6,0.2l-7.2,5.8c-1.8-2.1-3.8-4.1-6.1-5.8l5.5-7.4c0.3-0.5,0.2-1.1-0.2-1.4
								  c-2.8-2.1-5.9-4-9.2-5.5c-2-0.9-4-1.7-6-2.4c-0.1,0-0.2-0.1-0.3-0.1c-0.4,0-0.8,0.3-1,0.7l-2.9,8.7c-2.7-0.8-5.5-1.4-8.3-1.6
								  l0.7-9.2c0-0.3-0.1-0.5-0.2-0.7c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.1-0.4-0.1C140.8,0,139.6,0,138.3,0c-4.5,0-9,0.6-13.4,1.7
								  c-0.3,0.1-0.5,0.2-0.6,0.5c-0.1,0.2-0.2,0.5-0.1,0.8l2.3,8.9c-2.7,0.8-5.3,1.8-7.8,3.1l-4.4-8.1c-0.1-0.2-0.3-0.3-0.5-0.4
								  c-0.1-0.1-0.3-0.1-0.4-0.1c-0.2,0-0.3,0-0.5,0.1c-5.1,2.8-9.7,6.4-13.6,10.6c-0.4,0.4-0.4,1.1,0.1,1.4l6.8,6.2
								  c-1.8,2.1-3.5,4.4-4.9,6.9l-8.1-4.4c0,0,0,0,0,0c-0.1-0.1-0.3-0.1-0.4-0.1c-0.4,0-0.7,0.2-0.9,0.5c-0.4,0.8-0.9,1.7-1.3,2.5
								  c-2.1,4.4-3.5,9-4.3,13.7c0,0.3,0,0.5,0.2,0.8c0.1,0.1,0.2,0.3,0.4,0.3c0.1,0,0.2,0.1,0.3,0.1l9.1,1.5c-0.4,2.8-0.5,5.6-0.4,8.4
								  l-9.2,0.7c-0.3,0-0.5,0.1-0.7,0.4c-0.2,0.2-0.3,0.5-0.2,0.7c0.5,5.8,1.8,11.4,4.1,16.7c0.1,0.2,0.3,0.4,0.5,0.5
								  c0.1,0.1,0.3,0.1,0.4,0.1c0.1,0,0.3,0,0.4-0.1l8.5-3.6c1.2,2.6,2.6,5,4.2,7.3l-7.3,5.6c-0.2,0.2-0.4,0.4-0.4,0.7
								  c0,0.3,0,0.5,0.2,0.8c3.5,4.6,7.7,8.6,12.6,11.8c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.3,0.1,0.4,0.1c0.1,0,0.1,0,0.2,0
								  c0.3-0.1,0.5-0.2,0.7-0.4l5.1-7.7c1.5,1,3.1,1.8,4.7,2.6c0.9,0.4,1.9,0.8,2.8,1.2l-3.1,8.7c-0.2,0.5,0.1,1,0.5,1.3c0,0,0.1,0,0.1,0
								  c5.5,2,11.2,3,17,3.1c0,0,0,0,0,0c0.6,0,1-0.4,1-1l0.2-9.2c2.8,0,5.6-0.3,8.4-0.9l2.1,9c0.1,0.3,0.2,0.5,0.5,0.6c0,0,0.1,0,0.1,0.1
								  c0.1,0.1,0.3,0.1,0.4,0.1c0.1,0,0.2,0,0.2,0c5.6-1.3,11-3.5,15.9-6.6c0.2-0.1,0.4-0.4,0.5-0.6c0.1-0.3,0-0.5-0.1-0.8l-4.9-7.8
								  c2.4-1.5,4.6-3.3,6.6-5.3l6.6,6.4c0.1,0.1,0.2,0.1,0.3,0.2c0.1,0.1,0.3,0.1,0.4,0.1c0,0,0,0,0,0c0.3,0,0.5-0.1,0.7-0.3
								  c4-4.1,7.3-8.9,9.8-14.2c0.2-0.5,0-1.1-0.5-1.4l-8.3-3.9c1-2.2,1.8-4.6,2.4-6.9l9,2c0.1,0,0.2,0,0.2,0c0.2,0,0.4-0.1,0.5-0.2
								  c0.2-0.1,0.4-0.4,0.5-0.6c1.3-5.7,1.6-11.4,1-17.2C190.8,46.9,190.7,46.7,190.5,46.5z M162.7,64.3c-4.6,9.7-14.3,15.4-24.4,15.4
								  c-3.8,0-7.8-0.8-11.5-2.6c-13.4-6.3-19.2-22.4-12.8-35.8c4.6-9.7,14.3-15.4,24.4-15.4c3.8,0,7.8,0.8,11.5,2.6
								  C163.2,34.8,169,50.9,162.7,64.3z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M151.8,111.1c-0.5,0.1-0.9,0.2-1.4,0.2v91.4c0,1.6-1.3,3-3,3H8.9c-1.6,0-3-1.3-3-3V94.5h40.5
								  c4.9,0,8.9-4,8.9-8.9V43.7h23.9c0-0.3,0-0.6,0.1-0.9c0.3-1.7,0.7-3.4,1.1-5H51.1L0,88.9v113.7c0,4.9,4,8.9,8.9,8.9h138.6
								  c4.9,0,8.9-4,8.9-8.9v-92.8C154.9,110.3,153.4,110.7,151.8,111.1z M49.4,49.3v36.3c0,1.6-1.3,3-3,3H10.1L49.4,49.3z"/>
							</g>
							</svg>
						</div>
						<div class="icon-second">
							<div class="number-col first">
								<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span>
							</div>
							<div class="number-col second">
								<span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>1</span><span>0</span><span>0</span>
							</div>
						</div>
						<div class="icon-third" id="database">
							<svg version="1.1" id="Layer_database" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 191.1 211.5" enable-background="new 0 0 191.1 211.5" xml:space="preserve">
							<path fill="#0080FF" d="M142.8,58.2c0-5.9-7.4-10.5-22.5-13.8c-13.1-2.9-30.5-4.5-49-4.5s-35.8,1.6-49,4.5C7.4,47.7,0,52.2,0,58.2
								  c0,0.2,0,0.5,0,0.7c0,0.3,0,0.6,0,0.9v132.3c0,0.2,0,0.4,0,0.7c0,0.1,0,0.3,0,0.4c0,5.9,7.4,10.5,22.5,13.8
								  c13.1,2.9,30.5,4.5,49,4.5s35.8-1.6,49-4.5c15.1-3.4,22.5-7.9,22.5-13.8c0-0.2,0-0.4,0-0.6c0-0.2,0-0.3,0-0.5V59.8
								  c0-0.3,0-0.6,0-0.9C142.8,58.7,142.8,58.4,142.8,58.2z M23.9,50.5c12.7-2.8,29.6-4.4,47.6-4.4s34.9,1.6,47.6,4.4
								  c14.5,3.2,17.5,6.9,17.5,7.6s-3,4.4-17.5,7.6c-12.7,2.8-29.6,4.4-47.6,4.4s-34.9-1.6-47.6-4.4C9.3,62.6,6.3,59,6.3,58.2
								  S9.3,53.8,23.9,50.5z M119,200.8c-12.7,2.8-29.6,4.4-47.6,4.4s-34.9-1.6-47.6-4.4c-14.5-3.2-17.5-6.9-17.5-7.6
								  c0-0.1,0.1-0.3,0.2-0.5c-0.1-0.1-0.2-0.2-0.3-0.3c0-0.1,0.1-0.1,0.1-0.2c0,0,0-0.1,0-0.1V66.5c3.7,2.1,9.1,3.9,16.1,5.5
								  c13.1,2.9,30.5,4.5,49,4.5s35.8-1.6,49-4.5c7.1-1.6,12.4-3.4,16.1-5.5v125.6c0,0.8-0.3,1.4-0.7,2c0,0,0,0.1,0,0.1
								  C134.3,195.7,129.9,198.3,119,200.8z"/>
							</svg>
							<div id="sprocket">
								<img src="img/3_1.png" alt="">
							</div>
						</div>
					</div>
					<div class="desc-1st-party text-blue">
						<p>Platforma umożliwia przetwarzanie danych z witryn wydawcy, dostarczając informacji na temat ich użytkowników.</p>
						<p class="text-center">Zbieramy dane z zasięgu wydawcy</p>
						<p class="text-arrow">&#9660;</p>
						<p class="text-center">Dane trafiają do prywatnych repozytoriów</p>
						<p class="text-arrow">&#9660;</p>
						<p class="text-center">Dostarczamy informacje o użytkownikach</p>

						<p>Możliwości:</p>
						<ul>
							<li>Analiza i profilowanie użytkowników tylko na zasięgu wydawcy</li>
							<li>Szansa monetyzacji zasobów poprzez dostarczenie</li>
							<li>ich do zewnętrznych systemów emisji reklam</li>
						</ul>
						<p>Korzyści</p>
						<ul>
							<li>Optymalizacja przestrzeni reklamowej</li>
							<li>Wyższe przychody z emisji reklam</li>
							<li>Najwyższy poziom bezpieczeństwa danych</li>
						</ul>
					</div>
				</div>

				<div class="top-on-bottom blue">
					<div class="top-title">
						<h2>1st<br>party data</h2>
					</div>
				</div>
			</section>
			<div class="full-background blue"></div>
			<section id="slide4" class="slide">
				<div class="top white">
					<div class="top-title">
						<h2>3rd<br>party data</h2>
					</div>
				</div>
				<div class="main-container row">
					<div class="scheme-3rd-party">
						<img src="img/3rd-scheme.png" alt="">
					</div>
					<div class="desc-3rd-party text-white">
						<p>Platforma integruje dane z wielu źródeł i na ich podstawie dokonuje wielowymiarowej segmentacji użytkowników.</p>
						<p>Profile użytkowników są następnie monetyzowane na wiodących platformach w modelu Real-Time Bidding.</p>
						<p>Zbieramy dane na różnych witrynach internetowych > dane trafiają do wspólnego kontenera > Tworzymy profile użytkowników</p>
						<br>
						<p>Możliwości</p>
						<ul>
							<li>Odsprzedaż danych do platform reklamowych</li>
							<li>Elastyczna i skalowalna platforma danych</li>
							<li>Analiza i profilowanie użytkowników</li>
						</ul>
						<p>Korzyści</p>
						<ul>
							<li>Źródło dodatkowych przychodów dla wydawców</li>
							<li>Dostęp do danych z zewnętrznych źródeł</li>
							<li>Dokładniejsze profile użytkowników pozwalające na prowadzenie skutecznijeszych działań reklamowych w sieci</li>
					</div>
				</div>
			</section>
			<section id="slide5" class="slide">
				<div class="top blue">
					<div class="top-title">
						<h2>co nas wyróżnia</h2>
					</div>
				</div>
				<div class="main-container">
					<div class="col-md-4">
						<div class="icon">
							<svg version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 192.9 127.7" enable-background="new 0 0 192.9 127.7" xml:space="preserve">
							<g>
							</g>
							<g>
							<path fill="#0080FF" d="M174,0H18.9C8.5,0,0,8.5,0,18.9v89.9c0,10.4,8.5,18.9,18.9,18.9H174c10.4,0,18.9-8.5,18.9-18.9V18.9
								  C192.9,8.5,184.4,0,174,0z M185.5,108.8c0,6.3-5.2,11.5-11.5,11.5H18.9c-6.3,0-11.5-5.2-11.5-11.5V18.9c0-6.3,5.2-11.5,11.5-11.5
								  H174c6.3,0,11.5,5.2,11.5,11.5V108.8z"/>
							<polygon fill="#0080FF" points="29.8,30.3 30.9,35.1 36,32.6 36.1,32.6 36.1,54.2 42.3,54.2 42.3,27 37.1,27 	"/>
							<path fill="#0080FF" d="M61.2,26.5c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C71.4,33,68.5,26.5,61.2,26.5z M61.2,49.9L61.2,49.9c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C65,46.8,63.7,49.9,61.2,49.9z"/>
							<path fill="#0080FF" d="M84.5,26.5c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C94.6,33,91.7,26.5,84.5,26.5z M84.4,49.9L84.4,49.9c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C88.2,46.8,86.9,49.9,84.4,49.9z"/>
							<path fill="#0080FF" d="M107.7,26.5c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C117.9,33,115,26.5,107.7,26.5z M107.7,49.9L107.7,49.9c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C111.5,46.8,110.1,49.9,107.7,49.9z"/>
							<polygon fill="#0080FF" points="129,32.6 129.1,32.6 129.1,54.2 135.3,54.2 135.3,27 130,27 122.8,30.3 123.9,35.1 	"/>
							<path fill="#0080FF" d="M154.2,26.5c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C164.3,33,161.5,26.5,154.2,26.5z M154.2,49.9L154.2,49.9c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C158,46.8,156.6,49.9,154.2,49.9z"/>
							<polygon fill="#0080FF" points="29.8,80.6 30.9,85.4 36,82.9 36.1,82.9 36.1,104.5 42.3,104.5 42.3,77.3 37.1,77.3 	"/>
							<path fill="#0080FF" d="M61.2,76.8c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C71.4,83.3,68.5,76.8,61.2,76.8z M61.2,100.1L61.2,100.1c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C65,97,63.7,100.1,61.2,100.1z"/>
							<path fill="#0080FF" d="M84.5,76.8c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C94.6,83.3,91.7,76.8,84.5,76.8z M84.4,100.1L84.4,100.1c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C88.2,97,86.9,100.1,84.4,100.1z"/>
							<polygon fill="#0080FF" points="99.6,80.6 100.6,85.4 105.8,82.9 105.9,82.9 105.9,104.5 112,104.5 112,77.3 106.8,77.3 	"/>
							<path fill="#0080FF" d="M131,76.8c-7,0-10.3,6.2-10.3,14.1c0,7.7,3.1,14,10.2,14h0c6.9,0,10.3-5.8,10.3-14.2
								  C141.1,83.3,138.2,76.8,131,76.8z M130.9,100.1L130.9,100.1c-2.5,0-4-3-3.9-9.3c0-6.2,1.5-9.2,3.9-9.2c2.6,0,3.9,3.2,3.9,9.2
								  C134.7,97,133.4,100.1,130.9,100.1z"/>
							<polygon fill="#0080FF" points="146.1,80.6 147.1,85.4 152.3,82.9 152.4,82.9 152.4,104.5 158.5,104.5 158.5,77.3 153.3,77.3 	"/>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2 class="text-blue">100% pewności</h2>
							<p>Zbieramy wyłącznie tzw “twarde dane”<br>
								- nie przypisujemy cech osobom<br>
								na podstawie ich deklaracji<br>
								lub podobieństw do innych użytkowników.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="icon">
							<svg version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 256.6 259.2" enable-background="new 0 0 256.6 259.2" xml:space="preserve">
							<g>
							</g>
							<g>
							<path fill="none" stroke="#0080FF" stroke-width="7" stroke-miterlimit="10" d="M213.9,42.1c4.3-2,7.3-6.3,7.3-11.4
								  c0-6.9-5.6-12.5-12.5-12.5c-6.9,0-12.5,5.6-12.5,12.5c0,5,3,9.4,7.3,11.4c-10.7,3.4-18.7,16.9-18.7,33.1c0,1.5,0.1,3,0.2,4.5h47.4
								  c0.1-1.5,0.2-3,0.2-4.5C232.6,59,224.6,45.5,213.9,42.1z"/>
							<g>
							<g>
							<g>
							<rect x="110.5" y="121.8" fill="#0080FF" width="38" height="36.7"/>
							</g>
							<g>
							<path fill="#0080FF" d="M105.9,156.5c-1.9,0-3.5-1.6-3.5-3.5v-25.7c0-1.9,1.6-3.5,3.5-3.5s3.5,1.6,3.5,3.5V153
								  C109.4,154.9,107.8,156.5,105.9,156.5z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M153.3,156.5c-1.9,0-3.5-1.6-3.5-3.5v-25.7c0-1.9,1.6-3.5,3.5-3.5s3.5,1.6,3.5,3.5V153
								  C156.8,154.9,155.2,156.5,153.3,156.5z"/>
							</g>
							</g>
							<path fill="#0080FF" d="M111.1,101.4v16.2h38v-16.2H111.1z M120.4,114.1c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4s4.4,2,4.4,4.4
								  C124.8,112.2,122.9,114.1,120.4,114.1z M139.7,114.1c-2.4,0-4.4-2-4.4-4.4c0-2.4,2-4.4,4.4-4.4s4.4,2,4.4,4.4
								  C144.1,112.2,142.1,114.1,139.7,114.1z"/>
							<g>
							<circle fill="#0080FF" cx="129.6" cy="95.3" r="3.7"/>
							</g>
							</g>
							<g>
							<path fill="#0080FF" d="M240.1-0.6H106c-8.5,0-15.5,7-15.5,15.5v60.4c2.3-1.1,4.6-2.1,7-3V14.9c0-4.7,3.8-8.5,8.5-8.5h134.1
								  c4.7,0,8.5,3.8,8.5,8.5v142.9c0,4.7-3.8,8.5-8.5,8.5h-51.8c-1,2.4-2.1,4.7-3.3,7h55.1c8.5,0,15.5-7,15.5-15.5V14.9
								  C255.6,6.4,248.6-0.6,240.1-0.6z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M77.5,164.1c1.9,11.1,10.9,19.7,22.2,21C90.3,180.5,82.6,173.2,77.5,164.1z"/>
							<path fill="#0080FF" d="M161.1,172h-58.6c-6.7,0-12.1-5.4-12.1-12.1V99.3c-5.4,4.3-9.9,9.6-13.2,15.7v45c0,1.4,0.1,2.8,0.4,4.2
								  c5.1,9,12.8,16.4,22.2,21c0.9,0.1,1.9,0.2,2.8,0.2h41.6C150.7,182.1,156.4,177.6,161.1,172z"/>
							</g>
							<path fill="#0080FF" d="M15.5,259.2c-3.6,0-7.5-1.8-10.6-4.9c-2.4-2.4-4-5.2-4.6-8.1c-0.8-3.4,0-6.6,2.2-8.8l63.9-63.9
								  c-6.4-10.3-9.7-22.1-9.7-34.3c0-36.1,29.4-65.5,65.5-65.5s65.5,29.4,65.5,65.5s-29.4,65.5-65.5,65.5c-13.3,0-26-4-36.9-11.5
								  l-63.5,63.5C20.1,258.4,18,259.2,15.5,259.2z M122.1,81.9c-31.7,0-57.5,25.8-57.5,57.5c0,11.7,3.5,22.9,10.1,32.5l1.9,2.7
								  L8.1,243.2c0,0-0.2,0.5,0,1.4c0.3,1.4,1.2,2.9,2.5,4.1c1.9,1.9,3.8,2.5,4.9,2.5c0.4,0,0.6-0.1,0.6-0.1l68.4-68.4l2.8,2.1
								  c10.1,7.8,22.2,11.9,34.9,11.9c31.7,0,57.5-25.8,57.5-57.5S153.8,81.9,122.1,81.9z"/>
							<g>
							<path fill="#0080FF" d="M181.5,157.4l0.3,3.2h2.2c2.3-6.7,3.6-13.8,3.6-21.2c0-3.2-0.2-6.4-0.7-9.5c-3.5,6.3-5.6,14.3-5.6,22.8
								  C181.2,154.1,181.3,155.8,181.5,157.4z"/>
							<path fill="#0080FF" d="M220.6,118.8c2.6-2.9,4.1-6.7,4.1-10.7c0-8.8-7.2-16-16-16c-8.8,0-16,7.2-16,16c0,4,1.5,7.8,4.1,10.7
								  c-1.8,1.2-3.5,2.6-5.1,4.3c0.7,3.1,1.3,6.4,1.6,9.7c2.8-4.9,6.7-8.5,11.2-10l4.2-1.3l4.2,1.3c9.4,3,16.2,15.5,16.2,29.8
								  c0,0.3,0,0.7,0,1h-36.9c-0.5,2.4-1.1,4.7-1.8,7h45.2l0.3-3.2c0.2-1.6,0.2-3.3,0.2-4.8C236.1,137.9,229.9,124.9,220.6,118.8z
								  M212.5,116.3l-3.8,1.8l-3.8-1.8c-3.2-1.5-5.3-4.7-5.3-8.2c0-5,4.1-9,9-9c5,0,9,4.1,9,9C217.7,111.6,215.6,114.8,212.5,116.3z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M108.7,75.3c0,0.5,0,0.9,0,1.4c2.3-0.8,4.6-1.3,7-1.4c0,0,0,0,0-0.1c0-0.3,0-0.7,0-1
								  C113.3,74.4,111,74.8,108.7,75.3z"/>
							<path fill="#0080FF" d="M128.4,76.2c0.6,0.1,1.3,0.2,1.9,0.3c5.5,0.8,10.3,3.2,14.1,6.7h11.4c-5-3-10.5-5.4-16.3-7H128.4z"/>
							<path fill="#0080FF" d="M109.1,69.1c2.4-0.4,4.8-0.8,7.2-1c1.8-11.2,7.8-20.2,15.6-22.7l4.2-1.3l4.2,1.3
								  c9.4,3,16.2,15.5,16.2,29.8c0,0.3,0,0.7,0,1h-1c2.7,1.4,5.2,3,7.6,4.7l0.1-0.9c0.2-1.6,0.2-3.3,0.2-4.8c0-14.7-6.1-27.7-15.5-33.8
								  c2.6-2.9,4.1-6.7,4.1-10.7c0-8.8-7.2-16-16-16c-8.8,0-16,7.2-16,16c0,4,1.5,7.8,4.1,10.7C116.2,46.7,110.5,57,109.1,69.1z
								  M136.1,21.7c5,0,9,4.1,9,9c0,3.5-2.1,6.7-5.3,8.2l-3.8,1.8l-3.8-1.8c-3.2-1.5-5.3-4.7-5.3-8.2C127.1,25.7,131.1,21.7,136.1,21.7z
								  "/>
							</g>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2 class="text-blue">Antibot</h2>
							<p>Zbieramy wyłącznie tzw “twarde dane”<br>
								- nie przypisujemy cech osobom
								na podstawie ich deklaracji
								lub podobieństw do innych użytkowników.</p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="icon">
							<svg version="1.1" id="Layer_6" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 218 194.7" enable-background="new 0 0 218 194.7" xml:space="preserve">
							<g>
							</g>
							<g>
							<g>
							<path fill="#95989A" d="M147.8,159H146c-0.5,0-0.9-0.4-0.9-0.9c0-0.1,0-0.2,0-0.3l-1.6-2.8c-0.2-0.4-0.1-0.9,0.3-1.2
								  c0.4-0.2,0.9-0.1,1.2,0.3L147.8,159z M141.7,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9
								  S142.2,159,141.7,159z M133.1,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9S133.6,159,133.1,159z
								  M124.5,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9S124.9,159,124.5,159z M115.9,159h-4.3
								  c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9S116.3,159,115.9,159z M107.3,159H103c-0.5,0-0.9-0.4-0.9-0.9
								  s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9S107.7,159,107.3,159z M98.6,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3
								  c0.5,0,0.9,0.4,0.9,0.9S99.1,159,98.6,159z M90,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9H90c0.5,0,0.9,0.4,0.9,0.9
								  S90.5,159,90,159z M81.4,159h-4.3c-0.5,0-0.9-0.4-0.9-0.9s0.4-0.9,0.9-0.9h4.3c0.5,0,0.9,0.4,0.9,0.9S81.9,159,81.4,159z
								  M73.8,158.5c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7
								  C74.4,158.3,74.1,158.5,73.8,158.5z M142.2,151.8c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2
								  c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2C142.5,151.8,142.4,151.8,142.2,151.8z M78.1,151c-0.1,0-0.3,0-0.4-0.1
								  c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7C78.7,150.8,78.4,151,78.1,151z
								  M138,144.4c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2
								  C138.3,144.3,138.1,144.4,138,144.4z M82.3,143.5c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3
								  c0.4,0.2,0.6,0.8,0.3,1.2L83,143C82.9,143.3,82.6,143.5,82.3,143.5z M133.7,136.9c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7
								  c-0.2-0.4-0.1-0.9,0.3-1.2c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2C134,136.8,133.9,136.9,133.7,136.9z
								  M86.5,136c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7
								  C87.1,135.8,86.8,136,86.5,136z M129.5,129.4c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2c0.4-0.2,0.9-0.1,1.2,0.3
								  l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2C129.8,129.4,129.6,129.4,129.5,129.4z M90.7,128.5c-0.1,0-0.3,0-0.4-0.1
								  c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7C91.3,128.3,91,128.5,90.7,128.5z
								  M125.2,121.9c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7
								  c0.2,0.4,0.1,0.9-0.3,1.2C125.5,121.9,125.4,121.9,125.2,121.9z M95,121c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7
								  c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7C95.6,120.8,95.3,121,95,121z M121,114.4c-0.3,0-0.6-0.2-0.7-0.4
								  l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2C121.3,114.4,121.1,114.4,121,114.4z
								  M99.2,113.5c-0.1,0-0.3,0-0.4-0.1c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2L100,113
								  C99.8,113.3,99.5,113.5,99.2,113.5z M116.7,106.9c-0.3,0-0.6-0.2-0.7-0.4l-2.1-3.7c-0.2-0.4-0.1-0.9,0.3-1.2
								  c0.4-0.2,0.9-0.1,1.2,0.3l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2C117,106.9,116.9,106.9,116.7,106.9z M103.4,106c-0.1,0-0.3,0-0.4-0.1
								  c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7c0.2-0.4,0.8-0.6,1.2-0.3c0.4,0.2,0.6,0.8,0.3,1.2l-2.1,3.7C104,105.8,103.7,106,103.4,106z
								  M112.5,99.5c-0.3,0-0.6-0.2-0.7-0.4l-1.9-3.4l-1.4,2.4c-0.2,0.4-0.8,0.6-1.2,0.3c-0.4-0.2-0.6-0.8-0.3-1.2l2.1-3.7
								  c0.2-0.4,0.8-0.6,1.2-0.3c0.3,0.2,0.5,0.6,0.4,0.9c0.2,0.1,0.4,0.2,0.5,0.4l2.1,3.7c0.2,0.4,0.1,0.9-0.3,1.2
								  C112.8,99.4,112.6,99.5,112.5,99.5z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M144.7,96.8H72l-0.3-3.2c-0.2-2.2-0.3-4.5-0.3-6.6c0-20.9,9-39.1,22.5-47c-4.1-3.9-6.6-9.4-6.6-15.3
								  c0-11.6,9.4-21,21-21c11.6,0,21.1,9.4,21.1,21c0,5.8-2.5,11.3-6.6,15.3c13.5,7.8,22.5,26.1,22.5,47c0,2.1-0.1,4.4-0.3,6.6
								  L144.7,96.8z M78.5,89.8h59.8c0-0.9,0.1-1.9,0.1-2.8c0-20.5-9.9-38.6-23.7-43l-6.3-2l-6.3,2c-13.7,4.4-23.7,22.5-23.7,43
								  C78.4,87.9,78.4,88.8,78.5,89.8z M108.4,10.7c-7.7,0-14,6.3-14,14c0,5.4,3.2,10.4,8.2,12.7l5.9,2.7l5.9-2.7
								  c5-2.3,8.2-7.3,8.2-12.7C122.4,17,116.1,10.7,108.4,10.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M78,191.8H5.3L5,188.6c-0.2-2.2-0.3-4.5-0.3-6.6c0-20.9,9-39.2,22.5-47c-4.1-3.9-6.6-9.4-6.6-15.3
								  c0-11.6,9.4-21,21-21c11.6,0,21.1,9.4,21.1,21c0,5.8-2.5,11.3-6.6,15.3c13.5,7.8,22.5,26.1,22.5,47c0,2.1-0.1,4.4-0.3,6.6
								  L78,191.8z M11.7,184.8h59.8c0-0.9,0.1-1.9,0.1-2.8c0-20.5-9.9-38.6-23.7-43l-6.3-2l-6.3,2c-13.7,4.4-23.7,22.5-23.7,43
								  C11.7,182.9,11.7,183.8,11.7,184.8z M41.6,105.7c-7.7,0-14,6.3-14,14c0,5.4,3.2,10.4,8.2,12.7l5.9,2.7l5.9-2.7
								  c5-2.3,8.2-7.3,8.2-12.7C55.7,112,49.4,105.7,41.6,105.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M214.7,191.8H142l-0.3-3.2c-0.2-2.2-0.3-4.5-0.3-6.6c0-20.9,9-39.2,22.5-47c-4.1-3.9-6.6-9.4-6.6-15.3
								  c0-11.6,9.4-21,21-21c11.6,0,21.1,9.4,21.1,21c0,5.8-2.5,11.3-6.6,15.3c13.5,7.8,22.5,26.1,22.5,47c0,2.1-0.1,4.4-0.3,6.6
								  L214.7,191.8z M148.4,184.8h59.8c0-0.9,0.1-1.9,0.1-2.8c0-20.5-9.9-38.6-23.7-43l-6.3-2l-6.3,2c-13.7,4.4-23.7,22.5-23.7,43
								  C148.3,182.9,148.4,183.8,148.4,184.8z M178.3,105.7c-7.7,0-14,6.3-14,14c0,5.4,3.2,10.4,8.2,12.7l5.9,2.7l5.9-2.7
								  c5-2.3,8.2-7.3,8.2-12.7C192.4,112,186.1,105.7,178.3,105.7z"/>
							</g>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2 class="text-blue">User graph</h2>
							<p>Analizujemy relacje<br>
								występujące między użytkownikami<br>
								i potrafimy dotrzeć do osób<br>
								z ich najbliższego otoczenia.</p>
						</div>
					</div>
				</div>
			</section>
			<section id="slide6" class="slide">
				<div class="top gray">
					<div class="top-title">
						<h2>liczby</h2>
					</div>
				</div>
				<div class="main-container">
					<h2 class="text-center">Pierwsza i najwięszka platforma w CEE</h2>
					<div class="col-md-3">
						<div class="icon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 198.3 197" enable-background="new 0 0 198.3 197" xml:space="preserve">
							<g>
							</g>
							<g>
							<g>
							<g>
							<g>
							<g>
							<path fill="#0080FF" d="M99.1,196.2c-53.9,0-97.7-43.8-97.7-97.7c0-53.9,43.8-97.7,97.7-97.7c53.9,0,97.7,43.8,97.7,97.7
								  C196.8,152.4,153,196.2,99.1,196.2z M99.1,7C48.6,7,7.6,48,7.6,98.5c0,50.5,41,91.5,91.5,91.5c50.5,0,91.5-41,91.5-91.5
								  C190.6,48,149.5,7,99.1,7z"/>
							</g>
							</g>
							</g>
							</g>
							<g>
							<g>
							<path fill="#0080FF" d="M55.9,84.9c2.5-2.5,4-5.9,4-9.6c0-7.5-6.1-13.6-13.6-13.6c-7.5,0-13.6,6.1-13.6,13.6c0,3.6,1.5,7.1,4,9.6
								  c-8.4,5.1-14,16.6-14,29.7c0,1.4,0.1,2.8,0.2,4.2l0.2,2.3h46.5l0.2-2.3c0.1-1.4,0.2-2.8,0.2-4.2C69.9,101.5,64.4,90,55.9,84.9z
								  M60.9,97.7v18.4h-6.7V89.8C56.8,91.6,59.1,94.3,60.9,97.7z M45.7,116.1H39V89.4c1.1-0.7,2.2-1.3,3.4-1.6l3.3-1.1V116.1z
								  M46.5,86.6l3.6,1.2c1.1,0.3,2.1,0.8,3.1,1.4v26.9h-6.7V86.6z M54.1,78.7v-6.8c0.5,1,0.7,2.2,0.7,3.4
								  C54.8,76.5,54.6,77.6,54.1,78.7z M53.3,70.4v9.8c-0.9,1.2-2,2.2-3.4,2.9l-3.3,1.5V66.8C49.3,66.9,51.8,68.3,53.3,70.4z
								  M45.7,66.8v17.7l-3-1.4c-1.6-0.7-2.8-1.9-3.7-3.3v-8.9C40.4,68.6,42.8,67,45.7,66.8z M38.1,72.8v5.1c-0.3-0.8-0.4-1.7-0.4-2.6
								  C37.7,74.4,37.9,73.6,38.1,72.8z M38.1,90v26.1h-6.7V98.3C33.2,94.8,35.4,91.9,38.1,90z M27.7,114.6c0-5.3,1.1-10.3,2.9-14.6v16
								  h-2.9C27.7,115.6,27.7,115.1,27.7,114.6z M64.9,116.1h-3.2V99.4c2,4.4,3.2,9.7,3.2,15.3C64.9,115.1,64.9,115.6,64.9,116.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M108.7,84.9c2.5-2.5,4-5.9,4-9.6c0-7.5-6.1-13.6-13.6-13.6c-7.5,0-13.6,6.1-13.6,13.6
								  c0,3.6,1.5,7.1,4,9.6c-8.4,5.1-14,16.6-14,29.7c0,1.4,0.1,2.8,0.2,4.2l0.2,2.3h46.5l0.2-2.3c0.1-1.4,0.2-2.8,0.2-4.2
								  C122.7,101.5,117.2,90,108.7,84.9z M116.4,104.7l-11.4,11.4H96l17.9-17.9C114.9,100.2,115.8,102.3,116.4,104.7z M113.5,97.4
								  l-18.7,18.7h-9.5l24.2-24.2C111,93.5,112.4,95.3,113.5,97.4z M95.2,87.8l3.9-1.2l3,0.9l-21.1,21.1C82.5,98.4,88,90.1,95.2,87.8z
								  M107.6,75.3c0,3.3-2,6.4-5,7.8l-3.6,1.7l-2.6-1.2l10.8-10.8C107.5,73.6,107.6,74.4,107.6,75.3z M106.9,71.9L95.6,83.2l-0.2-0.1
								  c-2.1-1-3.7-2.8-4.4-4.9L102,67.3C104.2,68.1,106,69.7,106.9,71.9z M99.1,66.8c0.7,0,1.4,0.1,2,0.3L90.8,77.3
								  c-0.2-0.6-0.3-1.3-0.3-2C90.5,70.6,94.4,66.8,99.1,66.8z M80.5,114.6c0-1.6,0.1-3.2,0.3-4.7l22.2-22.2l0,0
								  c2.1,0.7,4.1,1.9,5.9,3.6l-24.8,24.8h-3.6C80.5,115.6,80.5,115.1,80.5,114.6z M117.7,116.1h-11.5l10.5-10.5
								  c0.7,2.9,1.1,5.9,1.1,9.1C117.7,115.1,117.7,115.6,117.7,116.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M161.5,84.9c2.5-2.5,4-5.9,4-9.6c0-7.5-6.1-13.6-13.6-13.6c-5.4,0-10.1,3.2-12.3,7.8c-0.1,0-0.1,0-0.2,0
								  l0.1,0.1c-0.8,1.7-1.2,3.6-1.2,5.7c0,3.6,1.5,7.1,4,9.6c-8.4,5.1-14,16.6-14,29.7c0,1.4,0.1,2.8,0.2,4.2l0.2,2.3h46.5l0.2-2.3
								  c0.1-1.4,0.2-2.8,0.2-4.2C175.6,101.5,170,90,161.5,84.9z M134.3,105.5c0.6-2.4,1.4-4.6,2.3-6.6l3.8,3.8l-4.5,4.5L134.3,105.5z
								  M135.4,107.8l-1.8,1.8c0.1-1,0.3-2,0.5-3L135.4,107.8z M148.2,67.6l3,3l-4.8,4.8l-2.6-2.6C144.5,70.5,146.1,68.6,148.2,67.6z
								  M160.4,75.3c0,1-0.2,2-0.5,2.9l-2.2-2.2l2.5-2.5C160.4,74,160.4,74.7,160.4,75.3z M166.6,98l-4.2,4.2l-4.8-4.8l4.8-4.8l2.3,2.3
								  C165.4,95.9,166.1,96.9,166.6,98z M155,116.1l-2.7-2.7l4.8-4.8l4.5,4.5l-3,3H155z M144.9,116.1l-3-3l4.5-4.5l4.8,4.8l-2.7,2.7
								  H144.9z M156.5,97.4l-4.8,4.8l-4.8-4.8l4.8-4.8L156.5,97.4z M148.1,87.8l3.8-1.2l3.6,1.1l-3.7,3.7L148.1,87.8z M152.3,92l4-4
								  c1.1,0.4,2.1,0.9,3.1,1.6l2.5,2.5l-4.8,4.8L152.3,92z M151.9,84.7l-2.1-1l1.9-1.9l2,2L151.9,84.7z M147.2,88.1l3.9,3.9l-4.8,4.8
								  l-4.8-4.8l1.8-1.8C144.6,89.3,145.9,88.6,147.2,88.1z M151.1,102.7l-4.8,4.8l-4.8-4.8l4.8-4.8L151.1,102.7z M147,108.1l4.8-4.8
								  l4.8,4.8l-4.8,4.8L147,108.1z M157.1,107.5l-4.8-4.8l4.8-4.8l4.8,4.8L157.1,107.5z M159.6,79c-0.8,1.8-2.3,3.2-4.1,4.1l-0.9,0.4
								  l-2.2-2.2l4.8-4.8L159.6,79z M151.7,80.7l-4.8-4.8l4.8-4.8l4.8,4.8L151.7,80.7z M151.1,81.3l-2.1,2.1l-0.7-0.3
								  c-1.9-0.9-3.4-2.4-4.2-4.2l2.3-2.3L151.1,81.3z M141,92.6l4.8,4.8l-4.8,4.8l-4-4c0.8-1.6,1.7-3,2.7-4.3L141,92.6z M141,103.3
								  l4.8,4.8l-4.5,4.5l-4.8-4.8L141,103.3z M143.7,116.1H139l2.4-2.4L143.7,116.1z M151.7,114l2.1,2.1h-4.1L151.7,114z M162.1,113.7
								  l2.4,2.4h-4.7L162.1,113.7z M162.1,112.5l-4.5-4.5l4.8-4.8l4.5,4.5L162.1,112.5z M163,102.7l4-4c1,2,1.8,4.2,2.4,6.6l-1.9,1.9
								  L163,102.7z M169.6,106.3c0.3,1.2,0.5,2.4,0.6,3.6l-2.1-2.1L169.6,106.3z M159.9,72.5l-2.8,2.8l-4.8-4.8l3.1-3.1
								  C157.5,68.5,159.2,70.3,159.9,72.5z M154.5,67.2l-2.8,2.8l-2.7-2.7c0.9-0.3,1.8-0.5,2.9-0.5C152.8,66.8,153.7,66.9,154.5,67.2z
								  M145.8,75.9l-2,2c-0.3-0.8-0.4-1.7-0.4-2.7c0-0.6,0.1-1.1,0.2-1.7L145.8,75.9z M133.3,114.6c0-1.3,0.1-2.5,0.2-3.8l2.5-2.5
								  l4.8,4.8l-3,3h-4.5C133.3,115.6,133.3,115.1,133.3,114.6z M170.5,116.1h-4.8l-3-3l4.8-4.8l2.8,2.8c0.1,1.1,0.2,2.3,0.2,3.4
								  C170.5,115.1,170.5,115.6,170.5,116.1z"/>
							</g>
							</g>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2>25M+</h2>
							<p>unikalnych użytkowników</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 198.3 197" enable-background="new 0 0 198.3 197" xml:space="preserve">
							<g>
							</g>
							<g>
							<g>
							<g>
							<g>
							<path fill="#0080FF" d="M99.2,196.2c-53.9,0-97.7-43.8-97.7-97.7c0-53.9,43.8-97.7,97.7-97.7s97.7,43.8,97.7,97.7
								  C196.9,152.4,153.1,196.2,99.2,196.2z M99.2,7C48.7,7,7.7,48,7.7,98.5C7.7,149,48.7,190,99.2,190c50.5,0,91.5-41,91.5-91.5
								  C190.7,48,149.6,7,99.2,7z"/>
							</g>
							</g>
							</g>
							</g>
							<g>
							<g>
							<g>
							<path fill="#0080FF" d="M131,146.3H61.1c-8,0-14.5-6.5-14.5-14.5v-31.3c0-8,6.5-14.5,14.5-14.5H131c8,0,14.5,6.5,14.5,14.5v31.3
								  C145.5,139.8,139,146.3,131,146.3z M61.1,91c-5.2,0-9.5,4.3-9.5,9.5v31.3c0,5.2,4.3,9.5,9.5,9.5H131c5.2,0,9.5-4.3,9.5-9.5v-31.3
								  c0-5.2-4.3-9.5-9.5-9.5H61.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M141.9,154H50.2c-1.4,0-2.5-1.1-2.5-2.5s1.1-2.5,2.5-2.5h91.7c1.4,0,2.5,1.1,2.5,2.5S143.3,154,141.9,154
								  z"/>
							</g>
							</g>
							<g>
							<g>
							<path fill="#0080FF" d="M65.7,124.8l-4-17.2h4.9l1,6.4c0.3,2,0.6,4.3,0.9,6h0.1c0.3-1.9,0.6-4,1-6.1l1.1-6.3h4.9l1.1,6.6
								  c0.3,2,0.5,3.7,0.8,5.7h0.1c0.3-1.9,0.6-4,0.9-6l1-6.3h4.7l-4.2,17.2h-5l-1.1-6.7c-0.3-1.5-0.5-3.1-0.7-5.1H73
								  c-0.3,2-0.5,3.6-0.8,5.1l-1.3,6.7H65.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M88.9,124.8l-4-17.2h4.9l1,6.4c0.3,2,0.6,4.3,0.9,6h0c0.3-1.9,0.6-4,1-6.1l1.1-6.3h4.9l1.1,6.6
								  c0.3,2,0.5,3.7,0.8,5.7h0.1c0.3-1.9,0.6-4,0.9-6l1-6.3h4.7l-4.2,17.2h-5l-1.1-6.7c-0.3-1.5-0.5-3.1-0.7-5.1h-0.1
								  c-0.3,2-0.5,3.6-0.8,5.1l-1.3,6.7H88.9z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M112,124.8l-3.9-17.2h4.9l1,6.4c0.3,2,0.6,4.3,0.9,6h0.1c0.3-1.9,0.6-4,1-6.1l1.1-6.3h4.9l1.1,6.6
								  c0.3,2,0.5,3.7,0.8,5.7h0.1c0.3-1.9,0.6-4,0.9-6l1-6.3h4.7l-4.2,17.2h-5l-1.1-6.7c-0.3-1.5-0.5-3.1-0.7-5.1h0
								  c-0.3,2-0.5,3.6-0.8,5.1l-1.3,6.7H112z"/>
							</g>
							</g>
							<g>
							<path fill="#0080FF" d="M39.9,125.5h2.7v-5h-2.7c-5.2,0-9.5-4.3-9.5-9.5V79.7c0-5.2,4.3-9.5,9.5-9.5h69.9c5.2,0,9.5,4.3,9.5,9.5
								  V82h5v-2.3c0-8-6.5-14.5-14.5-14.5H39.9c-8,0-14.5,6.5-14.5,14.5V111C25.4,119,31.9,125.5,39.9,125.5z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M42.2,128.6H30.1c-1.4,0-2.5,1.1-2.5,2.5s1.1,2.5,2.5,2.5h12.1V128.6z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M166.8,115h-17.6c0.1,1.7,0.1,3.3,0.2,5h17.4c1.4,0,2.5-1.1,2.5-2.5S168.1,115,166.8,115z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M155.9,52H86c-6.1,0-11.3,3.8-13.5,9.2h5.6c1.7-2.5,4.6-4.2,7.9-4.2h69.9c5.2,0,9.5,4.3,9.5,9.5v31.3
								  c0,5.2-4.3,9.5-9.5,9.5h-6.3v5h6.3c8,0,14.5-6.5,14.5-14.5V66.5C170.4,58.5,163.9,52,155.9,52z"/>
							</g>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2>500K+</h2>
							<p>witryn internetowych w zasigu</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 281.7 206" enable-background="new 0 0 281.7 206" xml:space="preserve">
							<g>
							</g>
							<g>
							<g>
							<path fill="#0080FF" d="M133.3,73.2c3.1-3.1,4.9-7.4,4.9-11.9c0-9.3-7.6-16.9-16.9-16.9c-9.3,0-16.9,7.6-16.9,16.9
								  c0,4.5,1.8,8.8,4.9,11.9C99,79.5,92.1,93.8,92.1,110c0,1.7,0.1,3.4,0.3,5.2l0.3,2.8h57.6l0.3-2.8c0.2-1.8,0.3-3.5,0.3-5.2
								  C150.7,93.8,143.8,79.5,133.3,73.2z M139.4,89v22.8h-8.3V79.2C134.4,81.5,137.2,84.9,139.4,89z M120.7,111.8h-8.3v-33
								  c1.3-0.9,2.8-1.6,4.2-2l4.1-1.3V111.8z M121.7,75.3l4.5,1.4c1.3,0.4,2.6,1,3.9,1.8v33.3h-8.3V75.3z M131.1,65.5v-8.4
								  c0.6,1.3,0.9,2.7,0.9,4.2C132,62.8,131.7,64.2,131.1,65.5z M130.1,55.3v12.1c-1.1,1.5-2.5,2.8-4.2,3.6l-4.1,1.9V50.8
								  C125.2,50.9,128.2,52.6,130.1,55.3z M120.7,50.8v21.9l-3.7-1.7c-1.9-0.9-3.5-2.4-4.6-4.1v-11C114.1,53,117.1,51,120.7,50.8z
								  M111.3,58.2v6.3c-0.3-1-0.5-2.1-0.5-3.2C110.8,60.2,111,59.2,111.3,58.2z M111.3,79.5v32.3H103V89.7
								  C105.1,85.4,108,81.9,111.3,79.5z M98.3,110c0-6.6,1.3-12.8,3.6-18.1v19.9h-3.6C98.3,111.2,98.3,110.6,98.3,110z M144.4,111.8
								  h-3.9V91.1c2.5,5.5,4,12,4,18.9C144.4,110.6,144.4,111.2,144.4,111.8z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M249.1,70.5c2.5-2.5,4-5.9,4-9.6c0-7.5-6.1-13.6-13.6-13.6c-5.4,0-10.1,3.2-12.3,7.8c-0.1,0-0.1,0-0.2,0
								  l0.1,0.1c-0.8,1.7-1.2,3.6-1.2,5.7c0,3.6,1.5,7.1,4,9.6c-8.4,5.1-14,16.6-14,29.7c0,1.4,0.1,2.8,0.2,4.2l0.2,2.3h46.5l0.2-2.3
								  c0.1-1.4,0.2-2.8,0.2-4.2C263.1,87.1,257.5,75.6,249.1,70.5z M221.9,91.1c0.6-2.4,1.4-4.6,2.3-6.6l3.8,3.8l-4.5,4.5L221.9,91.1z
								  M222.9,93.4l-1.8,1.8c0.1-1,0.3-2,0.5-3L222.9,93.4z M235.7,53.2l3,3l-4.8,4.8l-2.6-2.6C232,56.1,233.6,54.2,235.7,53.2z
								  M248,60.9c0,1-0.2,2-0.5,2.9l-2.2-2.2l2.5-2.5C247.9,59.6,248,60.3,248,60.9z M254.2,83.6l-4.2,4.2l-4.8-4.8l4.8-4.8l2.3,2.3
								  C253,81.5,253.6,82.5,254.2,83.6z M242.5,101.7l-2.7-2.7l4.8-4.8l4.5,4.5l-3,3H242.5z M232.4,101.7l-3-3l4.5-4.5l4.8,4.8l-2.7,2.7
								  H232.4z M244,83l-4.8,4.8l-4.8-4.8l4.8-4.8L244,83z M235.6,73.4l3.8-1.2l3.6,1.1l-3.7,3.7L235.6,73.4z M239.9,77.6l4-4
								  c1.1,0.4,2.1,0.9,3.1,1.6l2.5,2.5l-4.8,4.8L239.9,77.6z M239.4,70.4l-2.1-1l1.9-1.9l2,2L239.4,70.4z M234.7,73.7l3.9,3.9l-4.8,4.8
								  l-4.8-4.8l1.8-1.8C232.2,74.9,233.4,74.2,234.7,73.7z M238.7,88.3l-4.8,4.8l-4.8-4.8l4.8-4.8L238.7,88.3z M234.5,93.7l4.8-4.8
								  l4.8,4.8l-4.8,4.8L234.5,93.7z M244.6,93.1l-4.8-4.8l4.8-4.8l4.8,4.8L244.6,93.1z M247.1,64.6c-0.8,1.8-2.3,3.2-4.1,4.1l-0.9,0.4
								  l-2.2-2.2l4.8-4.8L247.1,64.6z M239.3,66.3l-4.8-4.8l4.8-4.8l4.8,4.8L239.3,66.3z M238.7,66.9l-2.1,2.1l-0.7-0.3
								  c-1.9-0.9-3.4-2.4-4.2-4.2l2.3-2.3L238.7,66.9z M228.6,78.2l4.8,4.8l-4.8,4.8l-4-4c0.8-1.6,1.7-3,2.7-4.3L228.6,78.2z M228.6,88.9
								  l4.8,4.8l-4.5,4.5l-4.8-4.8L228.6,88.9z M231.2,101.7h-4.7l2.4-2.4L231.2,101.7z M239.3,99.6l2.1,2.1h-4.1L239.3,99.6z
								  M249.7,99.3l2.4,2.4h-4.7L249.7,99.3z M249.7,98.1l-4.5-4.5l4.8-4.8l4.5,4.5L249.7,98.1z M250.6,88.3l4-4c1,2,1.8,4.2,2.4,6.6
								  l-1.9,1.9L250.6,88.3z M257.1,91.9c0.3,1.2,0.5,2.4,0.6,3.6l-2.1-2.1L257.1,91.9z M247.5,58.1l-2.8,2.8l-4.8-4.8l3.1-3.1
								  C245.1,54.1,246.7,55.9,247.5,58.1z M242.1,52.8l-2.8,2.8l-2.7-2.7c0.9-0.3,1.8-0.5,2.9-0.5C240.3,52.4,241.2,52.5,242.1,52.8z
								  M233.3,61.5l-2,2c-0.3-0.8-0.4-1.7-0.4-2.7c0-0.6,0.1-1.1,0.2-1.7L233.3,61.5z M220.8,100.3c0-1.3,0.1-2.5,0.2-3.8l2.5-2.5
								  l4.8,4.8l-3,3h-4.5C220.8,101.2,220.8,100.7,220.8,100.3z M258,101.7h-4.8l-3-3L255,94l2.8,2.8c0.1,1.1,0.2,2.3,0.2,3.4
								  C258,100.7,258,101.2,258,101.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M145.2,136H96c-2.3,0-4.1-1.8-4.1-4.1c0-2.3,1.8-4.1,4.1-4.1h49.3c2.3,0,4.1,1.8,4.1,4.1
								  C149.3,134.1,147.5,136,145.2,136z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M259.3,120.2h-39.7c-1.7,0-3.1-1.4-3.1-3.1c0-1.7,1.4-3.1,3.1-3.1h39.7c1.7,0,3.1,1.4,3.1,3.1
								  C262.4,118.8,261,120.2,259.3,120.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M259.3,132.1h-39.7c-1.7,0-3.1-1.4-3.1-3.1c0-1.7,1.4-3.1,3.1-3.1h39.7c1.7,0,3.1,1.4,3.1,3.1
								  C262.4,130.8,261,132.1,259.3,132.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M95,41.9c0.7-1.7,1.5-3.3,2.3-5c-3.9,2.6-8.1,4.7-12.8,6.5c-0.8,2.1-1.6,4.2-2.3,6.3
								  C86,46.6,90.3,43.9,95,41.9z"/>
							<path fill="#0080FF" d="M92,49.2C87.1,52,82.7,55.6,79,59.9c-3,11.5-4.7,23.6-4.7,36.1c0,8.7,0.8,17.3,2.4,25.6
								  c3.3,4.5,7.4,8.5,12.1,11.6c-3.5-11.8-5.4-24.3-5.4-37.2C83.4,79.5,86.5,63.7,92,49.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M113.2,32.7c1.3,0.7,2.2,1.9,2.6,3.3c1.1-1.2,2.2-2.3,3.4-3.4c-0.7,0-1.4-0.1-2.1-0.1
								  C115.7,32.5,114.4,32.6,113.2,32.7z"/>
							<path fill="#0080FF" d="M105.9,148.5c-0.4,0.4-0.9,0.7-1.5,0.9c0.8,0.2,1.6,0.3,2.4,0.5C106.5,149.4,106.2,148.9,105.9,148.5z"/>
							<path fill="#0080FF" d="M182.5,0.8c-24.9,0-47.6,9.3-64.8,24.7c3,0,6,0.3,8.9,0.7C142.1,14.1,161.5,7,182.5,7
								  C233,7,274,48,274,98.5C274,149,233,190,182.5,190c-27.9,0-52.9-12.5-69.7-32.3c-3-0.2-6-0.6-8.9-1.2
								  c17.8,24.1,46.4,39.7,78.6,39.7c53.9,0,97.7-43.8,97.7-97.7C280.2,44.6,236.4,0.8,182.5,0.8z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M17.2,203.2c-3.2,0-6.7-1.6-9.4-4.4c-2.7-2.7-4.4-6.3-4.4-9.4c0-2.2,0.7-4,2.1-5.4l60.2-60.2
								  c-6.1-9.7-9.3-20.7-9.3-32.2C56.4,58.2,83.6,31,117,31s60.6,27.2,60.6,60.6c0,33.4-27.2,60.6-60.6,60.6
								  c-12.5,0-24.4-3.8-34.6-10.9l-59.8,59.8C21.2,202.5,19.3,203.2,17.2,203.2z M117,37.2c-30,0-54.4,24.4-54.4,54.4
								  c0,11.1,3.3,21.7,9.6,30.8c0.8,1.2,0.7,2.9-0.4,3.9l-62,62c-0.3,0.3-0.3,0.8-0.3,1c0,1.1,0.7,3.2,2.6,5.1c1.9,1.9,3.9,2.6,5.1,2.6
								  c0.3,0,0.8,0,1-0.3l61.7-61.7c1.1-1.1,2.8-1.2,4.1-0.3c9.6,7.4,21,11.3,33.1,11.3c30,0,54.4-24.4,54.4-54.4
								  C171.5,61.6,147,37.2,117,37.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M207.3,114h-28c-0.8,2.1-1.6,4.2-2.6,6.2h30.6c1.7,0,3.1-1.4,3.1-3.1C210.4,115.4,209,114,207.3,114z"/>
							<path fill="#0080FF" d="M169.3,117.1c0,0.5,0.1,0.9,0.3,1.3c0.7-1.4,1.4-2.9,2-4.3C170.3,114.5,169.3,115.7,169.3,117.1z"/>
							</g>
							<path fill="#0080FF" d="M207,126h-33.3c-1.3,2.1-2.7,4.2-4.3,6.2H207c1.7,0,3.1-1.4,3.1-3.1S208.7,126,207,126z"/>
							<path fill="#0080FF" d="M196.3,70.5c2.5-2.5,4-5.9,4-9.6c0-7.5-6.1-13.6-13.6-13.6c-6.2,0-11.4,4.2-13.1,9.9
								  c3.2,5.2,5.7,10.9,7.3,17c0.6-0.3,1.2-0.6,1.8-0.8l3.9-1.2l3,0.9l-7.3,7.3c0.1,0.3,0.1,0.7,0.2,1l8-8l0,0c2.1,0.7,4.1,1.9,5.9,3.6
								  l-13.2,13.2c0,0.4,0,0.8,0,1.2L197,77.5c1.5,1.5,2.9,3.4,4,5.5l-18.6,18.6c-0.3,1.7-0.6,3.5-1,5.2h28.4l0.2-2.3
								  c0.1-1.4,0.2-2.8,0.2-4.2C210.3,87.1,204.7,75.6,196.3,70.5z M178,60.9c0-4.7,3.8-8.6,8.6-8.6c0.7,0,1.4,0.1,2,0.3l-10.3,10.3
								  C178.1,62.3,178,61.6,178,60.9z M183,68.7c-2.1-1-3.7-2.8-4.4-4.9l10.9-10.9c2.2,0.8,4,2.5,4.9,4.6l-11.3,11.3L183,68.7z
								  M190.2,68.7l-3.6,1.7l-2.6-1.2l10.8-10.8c0.3,0.8,0.4,1.7,0.4,2.5C195.2,64.2,193.2,67.3,190.2,68.7z M183.6,101.7l17.9-17.9
								  c1,2,1.8,4.1,2.5,6.5l-11.4,11.4H183.6z M205.2,101.7h-11.5l10.5-10.5c0.7,2.9,1.1,5.9,1.1,9.1
								  C205.2,100.7,205.2,101.2,205.2,101.7z"/>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2>5K+</h2>
							<p>analizowancyh zmiennych</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="icon">
							<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 841.9 595.3" enable-background="new 0 0 841.9 595.3" xml:space="preserve">
							<g>
							<g>
							<g>
							<g>
							<path fill="#0080FF" d="M421.1,592.9c-162.9,0-295.2-132.4-295.2-295.2S258.2,2.4,421.1,2.4s295.2,132.4,295.2,295.2
								  S584,592.9,421.1,592.9z M421.1,21.2c-152.6,0-276.5,123.9-276.5,276.5s123.9,276.5,276.5,276.5s276.5-123.9,276.5-276.5
								  S573.4,21.2,421.1,21.2z"/>
							</g>
							</g>
							</g>
							</g>
							<g>
							<g>
							<path fill="#0080FF" d="M527.1,100.5H323.3c-36.8,0-66.8,30-66.8,66.8V443c0,36.8,30,66.8,66.8,66.8h203.7
								  c36.8,0,66.8-30,66.8-66.8V167.3C593.8,130.4,563.9,100.5,527.1,100.5z M576.4,443c0,27.3-22.1,49.4-49.4,49.4H323.3
								  c-27.3,0-49.4-22.1-49.4-49.4V167.3c0-27.3,22.1-49.4,49.4-49.4h203.7c27.3,0,49.4,22.1,49.4,49.4V443z"/>
							</g>
							<g>
							<g>
							<path fill="#0080FF" d="M325.3,229.9c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C347.6,217.3,340.6,229.9,325.3,229.9L325.3,229.9z M325.5,218.1c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C318.7,212.9,321.3,218.1,325.5,218.1L325.5,218.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M370.9,186.2h-0.2l-11.2,5l-2.5-11.7l16.2-7.1H386v56.6h-15.1V186.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M425.1,229.9c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C447.4,217.3,440.4,229.9,425.1,229.9L425.1,229.9z M425.3,218.1c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4c-0.1,12.3,2.5,17.5,6.6,17.5H425.3z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M470.7,186.2h-0.2l-11.1,5l-2.5-11.7l16.2-7.1h12.8v56.6h-15.2V186.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M525,229.9c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C547.2,217.3,540.2,229.9,525,229.9L525,229.9z M525.1,218.1c4,0,6.5-5.3,6.5-17.5c0-11.9-2.3-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C518.3,212.9,520.9,218.1,525.1,218.1L525.1,218.1z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M325.3,334.4c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C347.6,321.8,340.6,334.4,325.3,334.4L325.3,334.4z M325.5,322.6c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C318.7,317.4,321.3,322.6,325.5,322.6L325.5,322.6z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M370.9,290.7h-0.2l-11.2,5l-2.5-11.8l16.2-7.1H386v56.6h-15.1V290.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M425.1,334.4c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C447.4,321.8,440.4,334.4,425.1,334.4L425.1,334.4z M425.3,322.6c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4c-0.1,12.3,2.5,17.5,6.6,17.5H425.3z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M475.1,334.4c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C497.3,321.8,490.3,334.4,475.1,334.4L475.1,334.4z M475.2,322.6c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C468.4,317.4,471,322.6,475.2,322.6L475.2,322.6z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M520.6,290.7h-0.2l-11.1,5l-2.5-11.8l16.2-7.1h12.8v56.6h-15.2V290.7z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M325.3,438.9c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C347.6,426.3,340.6,438.9,325.3,438.9L325.3,438.9z M325.5,427.2c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C318.7,421.9,321.3,427.2,325.5,427.2L325.5,427.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M370.9,395.2h-0.2l-11.2,5.1l-2.5-11.8l16.2-7.1H386v56.6h-15.1V395.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M420.8,395.2h-0.2l-11.1,5.1l-2.5-11.8l16.2-7.1h12.8v56.6h-15.1V395.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M475.1,438.9c-16.2,0-22.2-14.3-22.2-29.2c0-15.3,6.4-29.3,22.3-29.3c16.5,0,22.2,14.7,22.2,29.2
								  C497.3,426.3,490.3,438.9,475.1,438.9L475.1,438.9z M475.2,427.2c4,0,6.4-5.3,6.4-17.5c0-11.9-2.4-17.4-6.5-17.4
								  c-4,0-6.6,5.2-6.6,17.4C468.4,421.9,471,427.2,475.2,427.2L475.2,427.2z"/>
							</g>
							<g>
							<path fill="#0080FF" d="M520.6,395.2h-0.2l-11.1,5.1l-2.5-11.8l16.2-7.1h12.8v56.6h-15.2V395.2z"/>
							</g>
							</g>
							</g>
							</svg>
						</div>
						<div class="text-center">
							<h2>5TB+</h2>
							<p>dziennie przetwarzanych danych</p>
						</div>
					</div>
				</div>
			</section>
			<div class="full-background yellow"></div>
			<section id="slide7" class="slide">
				<div class="top white">
					<div class="top-title">
						<h2>profile<br>użytkowników</h2>
					</div>
				</div>
				<div class="main-container">
					<div id="user-scheme">
						<div class="circle main">
							<div class="icon">
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="0 0 251 107.3" enable-background="new 0 0 251 107.3" xml:space="preserve">
								<g>
								<g>
								<path fill="#FFFFFF" d="M57,42.7c3.9-3.9,6.2-9.3,6.2-15.1c0-11.8-9.6-21.4-21.4-21.4c-11.8,0-21.4,9.6-21.4,21.4
									  c0,5.7,2.3,11.1,6.2,15.1c-13.2,8-22,26.1-22,46.7c0,2.1,0.1,4.4,0.3,6.6l0.3,3.6h73l0.3-3.6c0.2-2.2,0.3-4.5,0.3-6.6
									  C79,68.8,70.2,50.7,57,42.7z M64.8,62.8v28.9H54.2V50.4C58.3,53.3,62,57.6,64.8,62.8z M41,91.7H30.4V49.8c1.7-1.1,3.5-2,5.4-2.6
									  l5.2-1.7V91.7z M42.3,45.4l5.7,1.8c1.7,0.5,3.3,1.3,4.9,2.3v42.2H42.3V45.4z M54.2,33V22.3c0.7,1.6,1.1,3.4,1.1,5.3
									  C55.3,29.5,54.9,31.3,54.2,33z M52.9,20v15.3c-1.3,1.9-3.2,3.5-5.4,4.5l-5.2,2.4v-28C46.7,14.4,50.5,16.6,52.9,20z M41,14.3v27.8
									  l-4.8-2.2c-2.5-1.1-4.5-3-5.8-5.2V20.7C32.6,17.1,36.5,14.6,41,14.3z M29.1,23.7v8c-0.4-1.3-0.7-2.7-0.7-4.1
									  C28.4,26.3,28.7,25,29.1,23.7z M29.1,50.7v41H18.5v-28C21.2,58.3,24.8,53.8,29.1,50.7z M12.6,89.4c0-8.3,1.7-16.2,4.6-22.9v25.2
									  h-4.5C12.6,90.9,12.6,90.2,12.6,89.4z M71,91.7h-5V65.4c3.2,6.9,5,15.2,5,24C71.1,90.2,71.1,90.9,71,91.7z"/>
								</g>
								<g>
								<path fill="#FFFFFF" d="M139.9,42.7c3.9-3.9,6.2-9.3,6.2-15.1c0-11.8-9.6-21.4-21.4-21.4c-11.8,0-21.4,9.6-21.4,21.4
									  c0,5.7,2.3,11.1,6.2,15.1c-13.2,8-22,26.1-22,46.7c0,2.1,0.1,4.4,0.3,6.6l0.3,3.6h73l0.3-3.6c0.2-2.2,0.3-4.5,0.3-6.6
									  C161.9,68.8,153.2,50.7,139.9,42.7z M152,73.7l-18,18h-14l28.1-28.1C149.7,66.7,151,70.1,152,73.7z M147.5,62.4l-29.3,29.3h-15
									  l37.9-37.9C143.5,56.2,145.7,59.1,147.5,62.4z M118.7,47.2l6.1-1.9l4.6,1.5L96.3,79.9C98.8,63.9,107.4,50.8,118.7,47.2z
									  M138.2,27.7c0,5.2-3.1,10-7.8,12.2l-5.6,2.6l-4.1-1.9l16.9-16.9C137.9,24.9,138.2,26.3,138.2,27.7z M137.1,22.3L119.4,40
									  l-0.2-0.1c-3.3-1.5-5.8-4.3-7-7.6l17.2-17.2C132.8,16.3,135.6,18.9,137.1,22.3z M124.8,14.2c1.1,0,2.1,0.2,3.1,0.4l-16.2,16.2
									  c-0.2-1-0.4-2.1-0.4-3.1C111.3,20.3,117.4,14.2,124.8,14.2z M95.5,89.4c0-2.5,0.2-5,0.4-7.4l34.9-34.9l0.1,0
									  c3.4,1.1,6.5,3,9.3,5.6l-38.9,38.9h-5.7C95.5,90.9,95.5,90.2,95.5,89.4z M154,91.7h-18.1l16.5-16.5c1.1,4.5,1.7,9.3,1.7,14.2
									  C154,90.2,154,90.9,154,91.7z"/>
								</g>
								<g>
								<path fill="#FFFFFF" d="M222.9,42.7c3.9-3.9,6.2-9.3,6.2-15.1c0-11.8-9.6-21.4-21.4-21.4c-8.5,0-15.9,5-19.3,12.3
									  c-0.1,0-0.2,0.1-0.2,0.1l0.1,0.1c-1.2,2.7-2,5.7-2,8.9c0,5.7,2.3,11.1,6.2,15.1c-13.2,8-22,26.1-22,46.7c0,2.1,0.1,4.4,0.3,6.6
									  l0.3,3.6h73l0.3-3.6c0.2-2.2,0.3-4.5,0.3-6.6C244.9,68.8,236.1,50.7,222.9,42.7z M180.1,75.1c0.9-3.7,2.1-7.2,3.6-10.4l5.9,5.9
									  l-7,7L180.1,75.1z M181.8,78.7l-2.8,2.8c0.2-1.6,0.5-3.2,0.8-4.8L181.8,78.7z M201.9,15.6l4.6,4.6l-7.5,7.5l-4.1-4.1
									  C196.1,20.1,198.6,17.2,201.9,15.6z M221.2,27.7c0,1.6-0.3,3.1-0.8,4.5l-3.5-3.5l4-4C221,25.6,221.2,26.6,221.2,27.7z M230.8,63.2
									  l-6.5,6.5l-7.5-7.5l7.5-7.5l3.7,3.7C229,60,230,61.6,230.8,63.2z M212.6,91.7l-4.2-4.2l7.5-7.5l7,7l-4.6,4.7H212.6z M196.7,91.7
									  l-4.6-4.7l7-7l7.5,7.5l-4.2,4.2H196.7z M215,62.3l-7.5,7.5l-7.5-7.5l7.5-7.5L215,62.3z M201.7,47.2l6-1.9l5.6,1.8l-5.9,5.9
									  L201.7,47.2z M208.4,53.9l6.3-6.3c1.7,0.6,3.3,1.4,4.8,2.4l3.9,3.9l-7.5,7.5L208.4,53.9z M207.7,42.5l-3.2-1.5l3-3l3.1,3.1
									  L207.7,42.5z M200.4,47.7l6.2,6.2l-7.5,7.5l-7.5-7.5l2.9-2.9C196.3,49.6,198.3,48.5,200.4,47.7z M206.5,70.7l-7.5,7.5l-7.5-7.5
									  l7.5-7.5L206.5,70.7z M200,79.1l7.5-7.5l7.5,7.5l-7.5,7.5L200,79.1z M215.9,78.2l-7.5-7.5l7.5-7.5l7.5,7.5L215.9,78.2z
									  M219.8,33.5c-1.3,2.8-3.6,5-6.4,6.4l-1.5,0.7l-3.5-3.5l7.5-7.5L219.8,33.5z M207.5,36.1l-7.5-7.5l7.5-7.5l7.5,7.5L207.5,36.1z
									  M206.5,37.1l-3.3,3.3l-1.1-0.5c-3-1.4-5.3-3.8-6.6-6.7l3.6-3.6L206.5,37.1z M190.6,54.8l7.5,7.5l-7.5,7.5l-6.2-6.2
									  c1.3-2.5,2.7-4.7,4.3-6.8L190.6,54.8z M190.6,71.7l7.5,7.5l-7,7l-7.5-7.5L190.6,71.7z M194.8,91.7h-7.4l3.7-3.7L194.8,91.7z
									  M207.5,88.5l3.2,3.2h-6.5L207.5,88.5z M223.8,88l3.7,3.7h-7.4L223.8,88z M223.8,86.1l-7-7l7.5-7.5l7,7L223.8,86.1z M225.2,70.7
									  l6.2-6.2c1.5,3.2,2.8,6.6,3.7,10.3l-2.9,2.9L225.2,70.7z M235.5,76.3c0.4,1.8,0.8,3.7,1,5.7l-3.3-3.3L235.5,76.3z M220.3,23.3
									  l-4.5,4.5l-7.5-7.5l4.8-4.8C216.6,16.9,219.1,19.8,220.3,23.3z M211.8,14.9l-4.4,4.4l-4.3-4.3c1.4-0.5,2.9-0.8,4.5-0.8
									  C209.2,14.2,210.5,14.5,211.8,14.9z M198.1,28.7l-3.2,3.2c-0.4-1.3-0.7-2.7-0.7-4.2c0-0.9,0.1-1.8,0.3-2.6L198.1,28.7z
									  M178.5,89.4c0-2,0.1-4,0.3-5.9l3.9-3.9l7.5,7.5l-4.6,4.6h-7C178.5,90.9,178.5,90.2,178.5,89.4z M236.9,91.7h-7.5l-4.6-4.6
									  l7.5-7.5l4.5,4.5c0.2,1.8,0.3,3.6,0.3,5.4C236.9,90.2,236.9,90.9,236.9,91.7z"/>
								</g>
								</g>
								</svg>
							</div>
							<div class="icon-desc number">25+M</div>
						</div>
						<div class="line first"></div>
						<div class="line second"></div>
						<div class="line third"></div>
						<div class="circle first">
							<div class="icon-desc">geolokalizacja</div>
							<div class="icon-desc hide">demografia</div>
						</div>
						<div class="circle second">
							<div class="icon-desc">demografia</div>
						</div>
						<div class="circle third">
							<div class="icon-desc">intencje zakupowe</div>
						</div>
						<div class="circle fourth">
							<div class="icon-desc">zainteresowania ogólne</div>
						</div>
						<div class="circle fifth">
							<div class="icon-desc">inne cechy</div>
						</div>
						<div class="circle sixth">
							<div class="icon-desc">social media</div>
						</div>
					</div>
				</div>
			</section>
			<section id="slide8" class="slide">
				<div class="top yellow">
					<div class="top-title">
						<h2>media o nas</h2>
					</div>
				</div>
				<div class="main-container">
					<div id="slideshow">
						<div id="sliders">
							<div id="slider1" class="slider">
								<div class="col-md-4 logo">
									<img src="img/forbes.png" alt="forbes" border="0">
								</div>
								<div class="col-md-7">
									<h2><a href="http://pierwszymilion.forbes.pl/specjalisci-od-skutecznej-reklamy,artykuly,176527,1,1.html" target="_blank">Specjaliści od skutecznej reklamy</a></h2>
								</div>
							</div>
							<div id="slider2" class="slider">
								<div class="col-md-4 logo">
									<img src="img/tvn.png" alt="tvn" border="0">
								</div>
								<div class="col-md-7">
									<h2><a href="" target="_blank">Lorem ipsum</a></h2>
									<h2><a href="" target="_blank">Spółka IT planuje wprowadzić swoje produkty do państw CEE</a></h2>
								</div>
							</div>
							<div id="slider3" class="slider">
								<div class="col-md-4 logo">
									<img src="img/wprost.png" alt="wprost" border="0">
								</div>
								<div class="col-md-7">
									<h2><a href="http://forum.wprost.pl/ar/469330/Reklamy-w-Internecie-beda-drozsze-niz-te-w-telewizji/" target="_blank">Reklamy w Internecie będą droższe niż te w telewizji</a></h2>
								</div>
							</div>
						</div>
					</div>
					<div id="arrows">
						<div class="arrow left">
							<svg version="1.1" id="LayerArrowL" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 76 46.6" enable-background="new 0 0 76 46.6" xml:space="preserve">
							<path fill="#FFFFFF" d="M39.3,2.3c0.8,0.5,1.3,1.3,1.3,2.2V17h31c1.4,0,2.5,1.1,2.5,2.5v7.6c0,1.4-1.1,2.5-2.5,2.5h-31v12.4
								  c0,0.9-0.5,1.8-1.3,2.2c-0.8,0.5-1.8,0.5-2.5,0L4.3,25.5C3.5,25.1,3,24.3,3,23.3c0-0.9,0.5-1.7,1.3-2.2L36.8,2.3
								  C37.5,1.9,38.5,1.9,39.3,2.3z"/>
							</svg>
						</div>
						<div class="arrow right">
							<svg version="1.1" id="LayerArrowR" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 76 46.6" enable-background="new 0 0 76 46.6" xml:space="preserve">
							<path fill="#FFFFFF" d="M37.8,2.3c-0.8,0.5-1.3,1.3-1.3,2.2V17h-31C4.1,17,3,18.1,3,19.5v7.6c0,1.4,1.1,2.5,2.5,2.5h31v12.4
								  c0,0.9,0.5,1.8,1.3,2.2c0.8,0.5,1.8,0.5,2.5,0l32.5-18.8c0.8-0.5,1.3-1.3,1.3-2.2c0-0.9-0.5-1.7-1.3-2.2L40.3,2.3
								  C39.5,1.9,38.6,1.9,37.8,2.3z"/>
							</svg>
						</div>
					</div>
					<div class="dots">
						<ul>
							<li class="active"></li>
							<li></li>
							<li></li>
						</ul>
					</div>
				</div>
			</section>
			<section id="slide9" class="slide row">
				<div class="top top-border white"></div>
				<div class="top blue">
					<div class="top-title">
						<h2>case studies</h2>
					</div>
				</div>
				<div class="full-size-container">
					<div class="left">
						<div class="circle">
							<img src="img/casestudy.jpg" alt="">
						</div>
						<div class="left-text">
							<h2>Jakiś tekst</h2>
							<h3>behavioralengine.com to największa polska platforma danych dla reklamy internetowej, która zasięgiem obejmuje ponad 20.000.000 użytkowników</h3>
						</div>
					</div>
					<div class="center">
						<ul>
							<li><img src="img/8_LOGO_1.png" alt=""></li>
							<li><img src="img/8_LOGO_2.png" alt=""></li>
							<li><img src="img/8_LOGO_3.png" alt=""></li>
							<li><img src="img/logo.png" alt=""></li>
						</ul>
					</div>
					<div class="right">
						<ul>
							<li><div class="icon"><img src="img/8_1.png" alt=""></div>
								<span class="number">-50%</span>
								16/21 segmentów zanotowało
								obniżenie eCPA o średnio 50%</li>
							<li><div class="icon"><img src="img/8_2.png" alt=""></div>
								<span class="number">85%</span>
								18/21 segmentów zanotowało
								wzrosty liczby konwersji</li>
							<li><div class="icon"><img src="img/8_3.png" alt=""></div>
								<span class="number">+50%</span>
								wzrost liczby konwersji</li>
							<li><div class="icon"><img src="img/8_4.png" alt=""></div>
								<span class="number">-33%</span>
								obniżenie kosztu konwersji</li>
							<li><div class="icon"><img src="img/8_5.png" alt=""></div>
								<span class="number">-48%</span>
								obniżenie kosztu za kliknięcie
							</li>
						</ul>
					</div>
					<br class="clearboth">
				</div>
			</section>
			<section id="slide10" class="slide">
				<div class="top gray">
					<div class="top-title">
						<h2>partnerzy</h2>
					</div>
				</div>
				<div class="main-container">
					<div id="isotop">
						<div class="item w2"><img src="img/amnet.png" alt=""></div>
						<div class="item navy-blue"><img src="img/oan.png" alt=""></div>
						<div class="item w1"><img src="img/goldbach.png" alt=""></div>
						<div class="item"><img src="img/adexon.png" alt=""></div>
						<div class="item"><img src="img/adform.png" alt=""></div>
						<div class="item w1 red"><img src="img/omd.png" alt=""></div>
						<div class="item w1"><img src="img/starcom.png" alt=""></div>
						<div class="item w1"><img src="img/doubleclick.png" alt=""></div>
						<div id="morePartners">
							Zobacz wszystkich partnerów
						</div>
					</div>
				</div>
			</section>
			<section id="slide11" class="slide">
				<div id="map-canvas"></div>
				<div id="contact">
					<div class="logo">
						<img src="img/logo.png" alt="Cloud Technologies">
					</div>
					<div id="contactForm">
						<h2>Formularz kontaktowy</h2>
						<form action="" method="post">
							<input type="text" name="name" id="name" required="required" placeholder="imię i nazwisko">
							<input type="email" name="email" id="email" required="required" placeholder="adres e-mail">
							<input type="tel" name="phone" id="phone" placeholder="numer telefonu">
							<input type="text" name="subject" id="subject" placeholder="tytuł wiadomości">
							<textarea name="description" id="description" required="required" placeholder="treść wiadomości"></textarea>
							<button type="submit" class="btn btn-primary">Wyślij</button>
						</form>
					</div>

				</div>
				<div class="top-on-bottom gray">
					<div class="top-title">
						Cloud Technologies S.A.<br>
						ul. Górskiego 6/24<br>
						00-033 Warszawa <br>
						Pn-Pt, 9:00-17:00<br>
						+48 225353050<br>
						<address>biuro@cloudtechnologies.pl</address>
					</div>
				</div>
			</section>
		</main>

		<?php
		// put your code here
		?>

		<!--<p>
			<span>1</span> 
			<span>0</span> 
			<span>1</span> 
			<span>1</span> 
			<span>1</span> 
			<span>0</span> 
			<span>0</span> 
			<span>1</span> 
			<span>0</span> 
			<span>0</span> 
			<span>1</span> 
			<span>0</span> 
			<span>1</span> 
			<span>1</span> 
			<span>0</span> 
			<span>1</span> 
			<span>1</span> 
			<span>1</span> 
			<span>0</span> 
			<span>0</span> 
		</p>

		<script>
			$(document).ready(function () {
				i = 1;
				elem = $('span');
				setInterval(function () {
					if (i - 5 > elem.length) {
						i = 1;
					}
					$('span:nth-child(' + (i - 5) + ')').animate({
						opacity: 1
					}, 500);
					$('span:nth-child(' + i + ')').animate({
						opacity: 0
					}, 500);
					i++;
				}, 100);
			});
		</script> -->

	</body>
</html>
