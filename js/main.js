/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* function polygon() {
 var svgns = "http://www.w3.org/2000/svg";
 var polygon = document.createElementNS(svgns, "polygon");
 var width = window.innerWidth / 50;
 polygon.setAttributeNS(null, "x", width);
 polygon.setAttributeNS(null, "y", 25);
 polygon.setAttributeNS(null, "fill", "green");
 document.documentElement.appendChild(polygon);
 }
 polygon(); */

// draw responsive triangle
function editTriangle() {
	var width = window.innerWidth / 2 - 120;
	var elems = document.getElementsByClassName('top');
	for (var i = 0; i < elems.length; i++) {
		elems[i].style.borderTopWidth = "160px";
		elems[i].style.borderRightWidth = width + 'px';
		elems[i].style.borderBottomWidth = "0";
		elems[i].style.borderLeftWidth = "120px";
	}
	var elemsBottom = document.getElementsByClassName('top-on-bottom');
	for (var i = 0; i < elemsBottom.length; i++) {
		elemsBottom[i].style.borderTopWidth = "160px";
		elemsBottom[i].style.borderRightWidth = width + 'px';
		elemsBottom[i].style.borderBottomWidth = "0";
		elemsBottom[i].style.borderLeftWidth = "120px";
	}
	var elemsBottom = document.getElementsByClassName('top-border');
	for (var i = 0; i < elemsBottom.length; i++) {
		elemsBottom[i].style.borderTopWidth = "168px";
		elemsBottom[i].style.borderRightWidth = width + 22 + 'px';
		elemsBottom[i].style.borderBottomWidth = "0";
		elemsBottom[i].style.borderLeftWidth = "126px";
	}
}

window.onload = editTriangle();
$(window).bind('resize', function (e)
{
	this.location.reload(false);
});
// if firefox
$(window).bind('resize', function (e)
{
	if (window.RT)
		clearTimeout(window.RT);
	window.RT = setTimeout(function ()
	{
		this.location.reload(false); /* false to get page from cache */
	}, 200);
});
$(document).ready(function () {

	//cloud number animation (intro)
	var speed = 1450;
	var times = 999;
	setTimeout(function () {
		var loop = setInterval(anim, 1500);
	}, 2000);
	function anim() {
		times--;
		if (times === 0) {
			clearInterval(loop);
		}
		var topColumn = parseInt($('.number-col.current').css('top'));
		var topColumn2 = parseInt($('.number-col.next').css('top'));

		if (topColumn < 0) {
			topColumn = 220;
			$('.number-col.current').css('top', '220px');
		}

		$('.number-col.current').animate({top: topColumn - 220}, speed, "linear");

		if (topColumn2 < 0) {
			topColumn2 = 220;
			$('.number-col.next').css('top', '220px');
		}

		$('.number-col.next').animate({top: topColumn2 - 220}, speed, "linear");
	}

// nav add action after click
	$('nav li a').click(function (e) {
		var $place = $(this).attr('href');
		var $scrollto = $($place).offset().top;
		$('html, body').animate({scrollTop: $scrollto}, 700, function () {
			window.location.hash = $place;
		});
		/* $('nav li a').removeClass('active');
		 $(this).addClass('active'); */
		e.preventDefault();
	});
	// slider
	var current = 1;
	var next = 2;
	var prev = 0;
	// $(".slider").css("display", "none"); // slides hide

	$("#slider1").show();
	var imgsize = $("#slideshow .slider").size();
	$(".arrow.right").click(function (evt) {

		if (next > imgsize) {
			next = 1;
			$(".slider").animate({
				"margin-left": "0"
			}, 500);
		}
		else {
// $(".slider").hide();
// $("#slider" + next).show();
			$("#slider" + current).animate({
				"margin-left": "+=-1200px"
			}, 500);
		}

		$(".dots li.active").removeClass("active");
		$(".dots li:nth-child(" + next + ")").addClass("active");
		current = next;
		next = next + 1;
		prev = next - 2;
		evt.preventDefault();
	});
	$(".arrow.left").click(function (evt) {

		if (prev < 1) {
			prev = imgsize;
		}

		$(".slider").hide();
		$("#slider" + prev).show();
		$(".dots li.active").removeClass("active");
		$(".dots li#" + prev).addClass("active");
		current = prev;
		prev = prev - 1;
		next = prev + 2;
		evt.preventDefault();
	});
	$(".dots li").click(function () {

		$(".dots li.active").removeClass("active");
		$(this).addClass("active");
		var element = $(".dots li").index(this);
		var element_index = element + 1;
		$(".slider").hide();
		$("#slider" + element_index).show();
		next = element_index + 1;
	});
	// hide circles
	function hideUserScheme() {
		$('#user-scheme .circle').not('.main').css({
			top: '135px',
			left: '385px',
			width: '10px',
			height: '10px'
		});
		$('#user-scheme .circle.main').css({
			width: '300px',
			height: '300px',
			marginTop: '-50px',
			marginLeft: '-125px',
			paddingTop: '45px'
		});
		$('#user-scheme .line').hide();
	}
	function animUserScheme() {
		$('#user-scheme .line').fadeIn('slow');
		$('#user-scheme .circle.main').css({
			width: '200px',
			height: '200px',
			marginTop: '0px',
			marginLeft: '-75px',
			paddingTop: '0'
		});
		$('#user-scheme .circle.first').css({
			left: '20px',
			top: '180px',
			width: '150px',
			height: '150px'
		});
		$('#user-scheme .circle.second').css({
			left: '260px',
			top: '0',
			width: '120px',
			height: '120px'
		});
		$('#user-scheme .circle.third').css({
			left: '600px',
			top: '-10px',
			width: '120px',
			height: '120px'
		});
		$('#user-scheme .circle.fourth').css({
			left: '700px',
			top: '95px',
			width: '150px',
			height: '150px'
		});
		$('#user-scheme .circle.fifth').css({
			left: '570px',
			top: '250px',
			width: '120px',
			height: '120px'
		});
		$('#user-scheme .circle.sixth').css({
			left: '260px',
			top: '290px',
			width: '120px',
			height: '120px'
		});
	}
	
	//hover in user scheme on slide4
	$('#user-scheme .circle.main').bind({
		mouseenter: function(e) {
			$(this).addClass('chover');
			$(this).children('.number').fadeIn();
		},
		mouseleave: function(e) {
			$(this).removeClass('chover');
			$(this).children('.number').fadeOut();
		}
	});

	// active element after scroll
	var startScrolling = $('main').offset().top;
	var slide1 = $('#slide1').offset().top;
	var slide2 = $('#slide2').offset().top - 10;
	var slide3 = $('#slide3').offset().top - 10;
	var slide4 = $('#slide4').offset().top - 10;
	var slide5 = $('#slide5').offset().top - 10;
	var slide6 = $('#slide6').offset().top - 10;
	var slide7 = $('#slide7').offset().top - 10;
	var slide8 = $('#slide8').offset().top - 10;
	var slide9 = $('#slide9').offset().top - 10;
	var slide10 = $('#slide10').offset().top - 10;
	var slide11 = $('#slide11').offset().top - 200;
	var database = $('#database').offset().top - 200;
	var cloud = $('#cloud svg').offset().top + 50;
	hideUserScheme();
	$(document).scroll(function (e) {
		var scrolling = $(document).scrollTop();
		var topdatabase = scrolling - database;
		if (topdatabase > 0 && scrolling < slide4) {
			if (topdatabase > 305) {
				$('#database').css("top", "1120px");
				$('#sprocket img').addClass('sprocket-rotate');
			}
			else {
				$('#database').css("top", 505 + topdatabase * 2 + "px");
				$('#sprocket img').removeClass('sprocket-rotate');
			}
			if (topdatabase > 170) {
				$('#database svg path').css("fill", "#fff");
			}
			else {
				$('#database svg path').css("fill", "#0080ff");
			}
		}
		if (scrolling > cloud) {
			$('.number-cols').fadeOut();
			if (scrolling > 600) {
				$('#cloud svg').css("top", '800px');
			}
			else {
				$('#cloud svg').css("top", +(scrolling - 50) * 1.45 + "px");
			}
		}
		else {
			$('#cloud svg').css("top", '0');
			$('.number-cols').fadeIn();
		}
		if (topdatabase < 0) {
			$('#database').css("top", "505px");
		}
		if (scrolling) {
			$('nav ul li a').removeClass('active');
		}
		if (scrolling >= slide1 && scrolling < slide2) {
			$('nav ul li:nth-child(1) a').addClass('active');
		}
		if (scrolling >= slide2 && scrolling < slide3) {
			$('nav ul li:nth-child(2) a').addClass('active');
		}
		if (scrolling >= slide3 && scrolling < slide4) {
			$('nav ul li:nth-child(3) a').addClass('active');
		}
		if (scrolling >= slide4 && scrolling < slide5) {
			$('nav ul li:nth-child(4) a').addClass('active');
		}
		if (scrolling >= slide5 && scrolling < slide6) {
			$('nav ul li:nth-child(5) a').addClass('active');
		}
		if (scrolling >= slide6 && scrolling < slide7) {
			$('nav ul li:nth-child(6) a').addClass('active');
		}
		if (scrolling >= slide7 && scrolling < slide8) {
			$('nav ul li:nth-child(7) a').addClass('active');
			animUserScheme();
		}
		else {
			hideUserScheme();
		}
		if (scrolling >= slide8 && scrolling < slide9) {
			$('nav ul li:nth-child(8) a').addClass('active');
		}
		if (scrolling >= slide9 && scrolling < slide10) {
			$('nav ul li:nth-child(9) a').addClass('active');
		}
		if (scrolling >= slide10 && scrolling < slide11) {
			$('nav ul li:nth-child(10) a').addClass('active');
		}
		if (scrolling >= slide11) {
			$('nav ul li:nth-child(11) a').addClass('active');
		}
	});
});